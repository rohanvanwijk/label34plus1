import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import prompt, { imagePrompt } from "./prompt.js";

// Define __dirname and __filename for ES modules
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

const getTemplates = async (dir) => {
    const directories = fs.readdirSync(dir, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name);

    const templates = {};

    for (const directory of directories) {
        const templatePath = path.join(dir, directory, 'template.html');
        if (fs.existsSync(templatePath)) {
            const templateContent = fs.readFileSync(templatePath, 'utf-8');
            templates[directory] = templateContent;
        }
    }

    return templates;
};

const getComponents = async (dir) => {
    const directories = fs.readdirSync(dir, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name);

    const components = [];

    for (const directory of directories) {
        const dataPath = path.join(dir, directory, 'data.json');
        if (fs.existsSync(dataPath)) {
            const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'));
            components.push(data);
        }
    }

    return components;
};

const generatePage = async (userPrompt) => {
    const componentLibrary = await getComponents('./components');

    console.log('[LOG] Generating page layout...');
    const response = await prompt(`
      Je krijgt een array van objecten die componenten voor een website vertegenwoordigen. Je taak is om een pagina te bouwen met deze componenten die overeenkomt met de gegeven wensen voor die specifieke pagina. We verwachten dat je een array van objecten retourneert met de naam 'page' in JSON-formaat met de componenten waaruit de pagina opgebouwd moet worden.

      Je MOET hierbij de volgende structuur aanhouden per component
      {
        "componentName": string,
        "description": string,
        "fields": {
          "name": string,
          "type": "string"|"image"|"array",
          "description": string,
          "value": "string"|{
            "name": string,
            "type": "string"|"image",
            "description": string,
            "value": string,
          }
        }[]
      }

      Je bent niet verplicht om alle componenten te gebruiken en je mag componenten naar wens hergebruiken. Zorg wel dat de pagina goed gevuld is, gebruik minimaal 5 componenten.

      Voor ieder veld van het type "image" moet de value een DALL-E prompt zijn van de afbeelding die je daar wil tonen. Het moet dus geen fictieve url worden, maar een gedetailleerd omschrijving van hoe de afbeelding eruit moet zien.

      Alle mogelijke componenten: ${JSON.stringify(componentLibrary)}

      Paginabeschrijving: ${userPrompt}
    `);

    let parsedResponse;
    try {
        parsedResponse = JSON.parse(response.content);
    } catch (e) {
        console.log(response.content);
        console.error("Kon antwoord niet als JSON parsen:", e);
        return;
    }

    const templates = await getTemplates('./components');
    let filledTemplates = [];
    let imagePromises = [];

    console.log('[LOG] Starting image generation');
    const processImageFields = async (fields) => {
        for (let i = 0; i < fields.length; i++) {
            if (fields[i].type === "image") {
                imagePromises.push(
                    imagePrompt(fields[i].value).then(imageResponse => {
                        console.log('[IMG] Finished generating an image');
                        fields[i].value = imageResponse;
                    }).catch(error => {
                        console.error('[ERR] Error generating image:', error);
                    })
                );
            } else if (fields[i].type === "array" || fields[i].type === "object") {
                processImageFields(fields[i].value);
            }
        }
    };

    for (let z = 0; z < parsedResponse.page.length; z++) {
        processImageFields(parsedResponse.page[z].fields);
    }

    await Promise.all(imagePromises);
    console.log('[LOG] Finished all images');

    console.log('[LOG] Preparing component templates');
    for (let i = 0; i < parsedResponse.page.length; i++) {
        const component = parsedResponse.page[i];
        const template = templates[component.componentName];
        if (template) {
            const filledTemplate = await prompt(`
                Gegeven de volgende template: ${template}
                En de volgende componentgegevens: ${JSON.stringify(component)}
                Vul de template in met de componentgegevens. Antwoord mij alleen met de ruwe html van de component met de ingevulde content. Niets anders. Plaats de code ook niet in markup. Alleen ruwe code.
            `, "text");
            filledTemplates.push(filledTemplate.content);
            console.log(`[LOG] Filled template ${i + 1} of ${parsedResponse.page.length} for component: ${component.componentName}`);
        } else {
            console.error(`[ERR] Failed to find template for component: ${component.componentName}`);
        }
    }

    const baseTemplatePath = path.join('./components', 'base.html');
    if (fs.existsSync(baseTemplatePath)) {
        const baseTemplate = fs.readFileSync(baseTemplatePath, 'utf-8');
        const fullContent = filledTemplates.join('\n');
        const finalPage = baseTemplate.replace('{{content}}', fullContent);
        fs.writeFileSync('./output/finalPage.html', finalPage, 'utf-8');
    } else {
        console.error('[ERR] Failed to find base template');
    }

    console.log('[LOG] Page generation finished');
};

app.post('/generate', async (req, res) => {
    const userPrompt = req.body.input;
    await generatePage(userPrompt);
    res.sendFile(path.join(__dirname, './output/finalPage.html'));
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});
