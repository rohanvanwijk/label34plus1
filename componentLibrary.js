const componentLibrary = [
  {
    componentName: "CallToAction",
    description:
      "Dit is een call-to-action component dat wordt gebruikt om gebruikers aan te moedigen een specifieke actie te ondernemen, zoals het bezoeken van een pagina of het kopen van een product.",
    fields: [
      {
        name: "title",
        type: "string",
        description: "De hoofdtekst of titel van de call-to-action.",
      },
      {
        name: "subtitle",
        type: "string",
        description:
          "Een aanvullende tekst of sub-titel voor extra context of details.",
      },
      {
        name: "buttonLabel",
        type: "string",
        description: "De tekst die op de knop wordt weergegeven.",
      },
      {
        name: "buttonLink",
        type: "string",
        description:
          "De URL waarnaar de gebruiker wordt doorgestuurd bij het klikken op de knop.",
      },
      {
        name: "image",
        type: "image",
        description:
          "De afbeelding die bij de call-to-action wordt weergegeven.",
      },
    ],
  },
  {
    componentName: "Hero",
    description:
      "Dit is een hero component dat wordt gebruikt als prominente banner bovenaan de pagina om belangrijke informatie te tonen en de aandacht van de gebruiker te trekken.",
    fields: [
      {
        name: "backgroundImage",
        type: "image",
        description: "De achtergrondafbeelding van de hero sectie.",
      },
      {
        name: "title",
        type: "string",
        description: "De hoofdtekst of titel van de hero sectie.",
      },
      {
        name: "subtitle",
        type: "string",
        description: "Een aanvullende tekst of sub-titel voor extra context.",
      },
      {
        name: "buttonLabel",
        type: "string",
        description: "De tekst die op de knop wordt weergegeven.",
      },
      {
        name: "buttonLink",
        type: "string",
        description:
          "De URL waarnaar de gebruiker wordt doorgestuurd bij het klikken op de knop.",
      },
    ],
  },
  {
    componentName: "FAQ",
    description:
      "Dit is een FAQ (Frequently Asked Questions) component dat wordt gebruikt om veelgestelde vragen en antwoorden weer te geven.",
    fields: [
      {
        name: "questions",
        type: "array",
        description: "Een lijst van vragen en antwoorden.",
        items: {
          type: "object",
          properties: {
            question: {
              type: "string",
              description: "De vraag.",
            },
            answer: {
              type: "string",
              description: "Het antwoord op de vraag.",
            },
          },
        },
      },
    ],
  },
  {
    componentName: "MediaAndText",
    description:
      "Dit is een Media & Text component dat wordt gebruikt om een combinatie van tekst en media, zoals afbeeldingen of video's, weer te geven.",
    fields: [
      {
        name: "media",
        type: "image",
        description: "De media (afbeelding of video) die wordt weergegeven.",
      },
      {
        name: "text",
        type: "string",
        description: "De tekst die naast de media wordt weergegeven.",
      },
      {
        name: "mediaPosition",
        type: "string",
        description:
          "De positie van de media ten opzichte van de tekst (bijv. 'left' of 'right').",
      },
    ],
  },
  {
    componentName: "Testimonial",
    description:
      "Dit is een testimonial component dat wordt gebruikt om klantrecensies of getuigenissen weer te geven.",
    fields: [
      {
        name: "quote",
        type: "string",
        description: "De tekst van de getuigenis of recensie.",
      },
      {
        name: "authorName",
        type: "string",
        description: "De naam van de persoon die de getuigenis heeft gegeven.",
      },
      {
        name: "authorImage",
        type: "image",
        description: "Een afbeelding van de auteur van de getuigenis.",
      },
      {
        name: "authorRole",
        type: "string",
        description:
          "De rol of functie van de auteur (bijv. 'CEO van Bedrijf X').",
      },
    ],
  },
];
export default componentLibrary;
