import OpenAI from "openai";

const openai = new OpenAI({
  apiKey: "sk-proj-B6cVN61AklhRgh5LBRgaT3BlbkFJbA5uSN4P4O5F1sGETVMC",
});

async function prompt(content, responseType = "json_object") {
  const chatCompletion = await openai.chat.completions.create({
    messages: [{ role: "user", content: content }],
    model: "gpt-4o",
    response_format: { type: responseType }
  });
  return chatCompletion.choices[0].message;
}

export async function imagePrompt(promptText) {
  const imageResponse = await openai.images.generate({
    prompt: promptText,
    n: 1,
    size: "1024x1024"
  });
  return imageResponse.data[0].url;
}

export default prompt;
